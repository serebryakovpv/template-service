## Сборка и запуск проекта
1. Сборка приложения требует генерации классов JOOQ
2. Сперва запусть базу
3. Настроить параметры подключения к БД в pom файле. Параметры используются и для flyway миграции и для кодогерации JOOQ.
   <db.driver>org.postgresql.Driver</db.driver>
   <db.url>jdbc:postgresql://localhost:5432/example</db.url>
   <db.username>example_user</db.username>
   <db.password>1</db.password>
4. Выполнить сборку mvn clean package

## Запуск БД

docker run --name mypgs -p 5432:5432 -d \
-e POSTGRES_DB=example \
-e POSTGRES_USER=example_user \
-e POSTGRES_PASSWORD=1 postgres

## ыВыполнение миграций flyway
Параметры подключения извлекаются из pom файла. Расположение миграций resources/db/migrations.

Накатить на БД
mvn flyway:migrate

Посмотреть какие миграции в БД
mvn flyway:info

Очистить всю БД
mvn flyway:clean

Проверить чек суммы миграций в БД и в исходниках
mvn flyway:validate

## Плагин кодогенерации jooq
mvn jooq-codegen:generate
Генерация происходит в пакет, настраиваемый параметром jooq.gen.packageName в pom файле.
Исходники:
1. коммитим в репозиторий, как в примере
2. в конвеере ci поднимаем базу и генерим каждый раз.

## Endpoints
### Actuator
только для отображения информации о миграциях в актуаторе
http://localhost:8081/example-service/actuator/flyway
### Swagger
http://localhost:8080/example-service/sui.html
