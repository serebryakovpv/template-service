package ru.template.service.exception

enum class ErrorCode{
    PROJECT_NOT_FOUND,
    PROJECT_EXIST
}