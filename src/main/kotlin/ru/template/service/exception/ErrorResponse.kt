package ru.template.service.exception

import io.swagger.v3.oas.annotations.media.Schema

@Schema
data class ErrorResponse(@Schema val code: ErrorCode, @Schema val message: String?)
