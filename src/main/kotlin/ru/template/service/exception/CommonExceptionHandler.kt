package ru.template.service.exception

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
/*  -- 200 - все ок { ....}
  -- 201 - использовать например в случае если объект реально создан.
  -- 500 сервис выводится из баланса
  -- 400   произошла лог ошибка: не прошла валидация данных 400.
         {
             code: ""
             message: ""
         }*/

@ControllerAdvice
class CommonExceptionHandler {
    val log = LoggerFactory.getLogger(this.javaClass)

    @ExceptionHandler
    fun handleCommonException(e: Exception): ResponseEntity<ErrorResponse>  {
        // логгируем ошибку и стек трейс для диагностики.
        log.error("Ошибка ", e)
        // возвращаем код 500 код и пустое body, по идее клиенту ни к чему знать почему упал сервис, к тому же в этом месте ответ может уходить в браузер
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @ExceptionHandler(ExampleException::class)
    fun handleBadRequest(e: ExampleException): ResponseEntity<ErrorResponse> {
        // При обработке внутреннего исключения стек трейс писать нет смысла,
        // по коду и тексту ошибки, должно четко определяться место и причина.
        log.error("Ошибка при обработке запроса. Код ответа {}, сообщение: {}", e.errorCode, e.message)
        return ResponseEntity.status(255)
            .body(ErrorResponse(e.errorCode, e.message));
    }
}