package ru.template.service.exception

import java.lang.RuntimeException

class ExampleException(val errorCode: ErrorCode, message: String, exception: Throwable? = null) :
    RuntimeException(message, exception)