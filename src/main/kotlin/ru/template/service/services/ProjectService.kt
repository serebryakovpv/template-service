package ru.template.service.services

import org.springframework.stereotype.Service
import ru.template.service.model.Project
import ru.template.service.model.criteria.Page
import ru.template.service.repositories.ProjectRepository

@Service
class ProjectService(val projectRepository: ProjectRepository) {
    fun getById(id: Long): Project = projectRepository.getById(id)
    fun getPage(page: Page):  List<Project> = projectRepository.getPage(page)
    fun insert(project: Project): Long = projectRepository.insert(project)
    fun update(project: Project) = projectRepository.update(project)
    fun delete(id: Long) = projectRepository.delete(id)
}