package ru.template.service.controller

import io.swagger.v3.oas.annotations.Operation
import ru.template.service.model.Project
import io.swagger.v3.oas.annotations.tags.Tag
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import ru.template.service.services.ProjectService
import ru.template.service.model.criteria.Page
import javax.validation.Valid
import javax.validation.constraints.NotNull

@RequestMapping("/api/v1/project")
@RestController
@Tag(name = "Управление проектами")
class ProjectController(val projectService: ProjectService) {
    val log = LoggerFactory.getLogger(this.javaClass)

    @Operation(summary = "Получить проект")
    @GetMapping("/{id}")
    fun getProject(@PathVariable id: Long): Project = projectService.getById(id)

    @Operation(summary = "Получить страницу с перечнем проектов")
    @GetMapping()
    fun getPage(@Valid page: Page): List<Project>{
        log.info("Запрос перечня проектов $page")
        return projectService.getPage(page)
    }

    @Operation(summary = "Добавить пользователя")
    @PostMapping
    fun addProject(@Valid @RequestBody project: Project): Long {
        val id = projectService.insert(project)
        log.info("Добавлен проект id=$id")
        return id
    }

    @Operation(summary = "Изменить проект")
    @PutMapping
    fun updateProject(@Valid @RequestBody project: Project) {
        projectService.update(project)
        log.info("Изменен проект $project")
    }

    @Operation(summary = "Удалить проект")
    @DeleteMapping("/{id}")
    fun deleteProject(@Valid @NotNull id: Long) {
        projectService.delete(id)
        log.info("Удален проект id=$id")
    }
}