package ru.template.service.repositories

import org.jooq.DSLContext
import org.springframework.dao.DuplicateKeyException
import org.springframework.stereotype.Repository
import ru.template.service.exception.ErrorCode
import ru.template.service.exception.ExampleException
import ru.template.service.model.Project
import ru.template.service.model.criteria.Page
import ru.template.service.repositories.dataase.Tables.PROJECT

@Repository
class ProjectRepository(private val dsl: DSLContext) {

    fun getById(id: Long): Project = dsl.selectFrom(PROJECT).where(PROJECT.ID.eq(id)).fetchOptional()
        .orElseThrow { ExampleException(ErrorCode.PROJECT_NOT_FOUND, "Не найден проект id=$id") }
        .into(Project::class.java)

    fun getPage(page: Page): List<Project> =
        dsl.selectFrom(PROJECT).offset(page.offset).limit(page.size).fetchInto(Project::class.java)

    fun insert(project: Project): Long{
        try {
            val projectRecord = dsl.newRecord(PROJECT)
            projectRecord.name = project.name
            projectRecord.store()
            return projectRecord.id
        }catch (e: DuplicateKeyException){
            throw ExampleException(ErrorCode.PROJECT_EXIST, "Проект с Id=${project.id} уже существует.")
        }
    }

    fun update(project: Project){
        val projectRecord = dsl.newRecord(PROJECT, project)
        projectRecord.update();
    }

    fun delete(id: Long) = dsl.deleteFrom(PROJECT).where(PROJECT.ID.eq(id))
}