package ru.template.service.model

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.*

@Schema
data class Project(
    @Schema(name = "ID")
    var id: Long?,

    @Schema(name = "Название")
    @field:Size(min = 2, max = 100)
    var name: String
)