package ru.template.service.model.criteria

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

data class Page(@field:NotNull @field:Min(0) val offset: Int, @field:NotNull @field:Min(1) val size: Int)
