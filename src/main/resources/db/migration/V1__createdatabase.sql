create table project
(
    id   bigserial primary key,
    name varchar(1000) not null
);

create table project_item
(
    id         bigserial primary key,
    name       varchar(500) not null,
    type       varchar(100) not null,
    project_id bigint references project (id)
);

insert into project(id, name) values (1, 'Проект "Рога и копыта"');
insert into project_item(id, name, type, project_id)
values (1, 'Сбор рогов', 'Рога', 1);
insert into project_item(id, name, type, project_id)
values (2, 'Сбор копыт', 'Копыта', 1);
